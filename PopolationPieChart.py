import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
import requests


link = requests.get('https://en.wikipedia.org/wiki/List_of_U.S._states_and_territories_by_population')

soup = BeautifulSoup(link.text, 'lxml')

"""This is getting the state names out of there"""

scrambled = soup.select('table')[0]

pop = scrambled.select('tbody tr td')

population = []
states = []
for i in range(2, len(pop), 9):
    #print(str(pop[i].text) + '\n')
    text = pop[i].text.strip()
  
    print(text)
    states.append(text)
    if 'Wyoming' in pop[i].text:
        break
for i in range(3, len(pop), 9):
    print(str(pop[i].text) + '\n')
    text = pop[i].text.replace(',', '')
    text = text.replace(' ', '')

    population.append(text)
    if 'Wyoming' in pop[i-1].text:
        break


plt.pie(population,labels=states,rotatelabels=45, shadow=True, startangle=90)#,autopct='%1.1f%%'


plt.show()










